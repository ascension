# Tests for Ascension

These tests are integration tests designed to run a local nameserver with
different zones and configurations.

Requirements for running these tests:

- Knot DNS server
