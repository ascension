Format: 3.0 (quilt)
Source: ascension
Binary: python3-ascension
Architecture: all
Version: 0.5.0-1
Maintainer: rexxnor <rexxnor+gnunet@brief.li>
Homepage: https://gnunet.org/git/ascension.git/
Standards-Version: 3.9.6
Build-Depends: dh-python, python3-setuptools, python3-all, debhelper (>= 9)
Package-List:
 python3-ascension deb python optional arch=all
Checksums-Sha1:
 d3e63b150d5f4edd8c893072874aab93cb4b36ac 9836 ascension_0.5.0.orig.tar.gz
 68622af6c1d2ed314ff13a977ec8b71afe3f24de 1792 ascension_0.5.0-1.debian.tar.xz
Checksums-Sha256:
 3959924e9ac60366fa682724c358eb96dbbff02071ef8db5ba0f2e9999e3baf2 9836 ascension_0.5.0.orig.tar.gz
 1352fb94269a102747e6bbd583ea1bb96795eecf9fac90855aba571dab2aeef4 1792 ascension_0.5.0-1.debian.tar.xz
Files:
 ad84dbb8c5b8236650fd9d71b759040d 9836 ascension_0.5.0.orig.tar.gz
 417a57f8ca7fedc227176f97c4ce3aa7 1792 ascension_0.5.0-1.debian.tar.xz
