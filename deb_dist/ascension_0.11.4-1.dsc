Format: 3.0 (quilt)
Source: ascension
Binary: python3-ascension
Architecture: all
Version: 0.11.4-1
Maintainer: rexxnor <rexxnor+gnunet@brief.li>
Standards-Version: 3.9.1
Build-Depends: python3-setuptools, python3-all, debhelper (>= 7.4.3)
Package-List:
 python3-ascension deb python optional arch=all
Checksums-Sha1:
 ea06751883f83324f4f94d2a2d14ae6e8fdf7993 10655 ascension_0.11.4.orig.tar.gz
 b56b1e763b686e4db61e4c12647d5425140247ff 1888 ascension_0.11.4-1.debian.tar.xz
Checksums-Sha256:
 69f7835f69ab067bf04cf95bfe9d84bff57aa9772dee087298f12bbcda2929f1 10655 ascension_0.11.4.orig.tar.gz
 2df89aa831d0d1f8f09df838d57b1a5ba29df643f29b1ad5cf3b96ca1b94b7ce 1888 ascension_0.11.4-1.debian.tar.xz
Files:
 737ce6949e7dee1aa3a3723908b36566 10655 ascension_0.11.4.orig.tar.gz
 dc259ee2481403186c7e4cf91ec0b9dc 1888 ascension_0.11.4-1.debian.tar.xz
