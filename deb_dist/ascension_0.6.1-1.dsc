Format: 3.0 (quilt)
Source: ascension
Binary: python3-ascension
Architecture: all
Version: 0.6.1-1
Maintainer: rexxnor <rexxnor+gnunet@brief.li>
Standards-Version: 3.9.1
Build-Depends: python3-setuptools, python3-all, debhelper (>= 7.4.3)
Package-List:
 python3-ascension deb python optional arch=all
Checksums-Sha1:
 5ac1866bc5647c4e1bae100106ee4a2cebba7c2a 10617 ascension_0.6.1.orig.tar.gz
 effc16e88124fb9142d73abc69282d66d480de65 1668 ascension_0.6.1-1.debian.tar.xz
Checksums-Sha256:
 7b197ce4a902c687c51456056655eb651ee924d62bfd6563b5a16ecfe5442f56 10617 ascension_0.6.1.orig.tar.gz
 8e9813eadff1fa287d0a418ba217495894aa738d7424dd817f1ec7a2c490220e 1668 ascension_0.6.1-1.debian.tar.xz
Files:
 4941444a2bbb07328aedddad8e65e136 10617 ascension_0.6.1.orig.tar.gz
 6a1364085319e8b6c71c0caaaf99e5a8 1668 ascension_0.6.1-1.debian.tar.xz
