Format: 3.0 (quilt)
Source: ascension
Binary: python3-ascension
Architecture: all
Version: 0.11.5-1
Maintainer: rexxnor <rexxnor+gnunet@brief.li>
Standards-Version: 3.9.1
Build-Depends: python3-setuptools, python3-all, debhelper (>= 7.4.3)
Package-List:
 python3-ascension deb python optional arch=all
Checksums-Sha1:
 8f3149f126141b96652799d1603bfa542ee8d428 11240 ascension_0.11.5.orig.tar.gz
 7a3e5b65230bc1383ab891542efe654c325b13c4 1668 ascension_0.11.5-1.debian.tar.xz
Checksums-Sha256:
 2e874e3159e02ad82a40f2d32173185a546fdcb1a349e048203b237bf9a5789b 11240 ascension_0.11.5.orig.tar.gz
 ce014ac1c00c226a5613b866bf6d3e11ecf81ecd0f99f408bca0fc84aafdfcac 1668 ascension_0.11.5-1.debian.tar.xz
Files:
 da11eee3599ed1d294dbbfdae6a10b4a 11240 ascension_0.11.5.orig.tar.gz
 4ae84d4412deb7d897e0ce0eb878b645 1668 ascension_0.11.5-1.debian.tar.xz
