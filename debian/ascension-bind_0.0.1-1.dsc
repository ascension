Format: 3.0 (quilt)
Source: ascension-bind
Binary: ascension-bind
Architecture: any
Version: 0.0.1-1
Maintainer: rexxnor <rexxnor+gnunet@brief.li>
Homepage: https://git.gnunet.org/ascension.git/
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9), python3-ascension, po-debconf
Package-List:
 ascension-bind deb net optional arch=any
Checksums-Sha1:
 0a7953cf5bf7616ff1c2171789fab736e199e769 3860 ascension-bind_0.0.1.orig.tar.xz
 0e0bde8e10468463ae3fcdb1574cf3477668daf9 4196 ascension-bind_0.0.1-1.debian.tar.xz
Checksums-Sha256:
 e21b0672b6d9932d03541c13e9350546542d9dd86373bf6bc78f61a1c79586b4 3860 ascension-bind_0.0.1.orig.tar.xz
 0854d2c10da448b0642124b4c890524b4ea536232e99b6bf832044846d2c0d04 4196 ascension-bind_0.0.1-1.debian.tar.xz
Files:
 3e51a0f28a46eff44e0366ab9185b840 3860 ascension-bind_0.0.1.orig.tar.xz
 553d863b9fddef218568c6242c13c118 4196 ascension-bind_0.0.1-1.debian.tar.xz
